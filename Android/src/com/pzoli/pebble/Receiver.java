package com.pzoli.pebble;

import java.io.IOException;
import java.util.UUID;

import org.json.JSONException;

import com.getpebble.android.kit.Constants;
import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

public class Receiver extends BroadcastReceiver {
	private final static UUID PEBBLE_APP_UUID = UUID.fromString("52D077F9-7B3D-4E46-B711-CEEFBE508F1B");
	public static MediaPlayer mPlayer = null;
	int CMD_KEY = 0x00;
	int MSG_PLAY_KEY = 0x00;
	int MSG_STOP_KEY = 0x00;
	
	@Override
	public void onReceive(Context context, Intent intent) {
	    final UUID receivedUuid = (UUID) intent.getSerializableExtra(Constants.APP_UUID);
		if (!intent.getAction().equals(Constants.INTENT_APP_RECEIVE) || !PEBBLE_APP_UUID.equals(receivedUuid))
			return;					            
	    
		if(mPlayer == null){
			mPlayer = MediaPlayer.create(context, R.raw.geci);
		}
	            
	    final int transactionId = intent.getIntExtra(Constants.TRANSACTION_ID, -1);
	    final String jsonData = intent.getStringExtra(Constants.MSG_DATA);
	    if (jsonData == null || jsonData.isEmpty()) {
	    	Log.d("PEBBLE: ","jsonData null");
	        return;
	    }
	                    
	    try {
	    	final PebbleDictionary data = PebbleDictionary.fromJson(jsonData);
	    	Log.d("PEBBLE: ",jsonData.toString());
	    	if(data.getUnsignedInteger(CMD_KEY).intValue() == MSG_PLAY_KEY)
	    	{
	    		Log.d("PEBBLE: ","Music started");
	    		prepareVolume(context);
	    		mPlayer.start();	    		
	    	}else{
	    		Log.d("PEBBLE: ","Music stopped");
	    		if(mPlayer.isPlaying()){
	    			mPlayer.stop();
	    			mPlayer.release();
	    			mPlayer = null;
	    		}
	    	}
	    	
	    } catch (JSONException e) {			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
	    PebbleKit.sendAckToPebble(context, transactionId);	   	    	    
	}
	
	private void prepareVolume(Context context){
		AudioManager audio = (AudioManager) context.getSystemService(context.AUDIO_SERVICE);

		//int currentVolume = audio.getStreamVolume(AudioManager.STREAM_RING);		
		int maxMusicVolume = audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		audio.setStreamVolume(AudioManager.STREAM_MUSIC, maxMusicVolume, 0);
	}
}
